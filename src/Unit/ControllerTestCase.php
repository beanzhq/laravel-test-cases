<?php

namespace Beanz\Tests\Unit;

use App\Models\User;
use Beanz\Tests\UnitTestCase;
use Illuminate\Testing\TestResponse;
use Illuminate\Support\Str;

abstract class ControllerTestCase extends UnitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $routeName;

    /** @var string */
    protected $redirectRouteName;

    /** @var string */
    protected $method;

    /** @var array */
    private $urlParameters;

    /** @var array */
    private $redirectParameters = [];

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->actingAs($this->user);
    }

    protected function callRoute(
        array $data = null,
        string $uri = null,
        string $method = null,
        array $files = [],
        array $headers = [],
        array $cookies = []
    ): TestResponse {
        $method = $method ?? $this->method;
        $data = $data ?? $this->getData();
        $uri = $uri ?? route($this->routeName, $this->urlParameters ?? []);

        $content = json_encode($data);

        $headers = array_merge(
            [
                'CONTENT_LENGTH' => Str::length($content, '8bit'),
                'CONTENT_TYPE' => 'application/json',
                'Accept' => 'application/json',
            ],
            $headers
        );

        return $this->call(
            $method,
            $uri,
            [],
            $cookies,
            $files,
            $this->transformHeadersToServerVars($headers),
            $content
        );
    }

    protected function jsonRoute(
        array $data = null,
        string $uri = null,
        string $method = null,
        array $headers = []
    ): TestResponse {
        $method = $method ?? $this->method;
        $data = $data ?? $this->getData();
        $uri = $uri ?? route($this->routeName, $this->urlParameters ?? []);

        return $this->json($method, $uri, $data, $headers);
    }


    protected function setUrlParameters(array $urlParameters = []): self
    {
        $this->urlParameters = $urlParameters;

        return $this;
    }

    protected function setRedirectParameters(array $urlParameters = []): self
    {
        $this->redirectParameters = $urlParameters;

        return $this;
    }

    protected function getUrlParameters(): array
    {
        return $this->urlParameters;
    }

    protected function getRedirectParameters(): array
    {
        return $this->redirectParameters;
    }

    protected function setRedirectRouteName(string $redirectRouteName): self
    {
        $this->redirectRouteName = $redirectRouteName;

        return $this;
    }

    protected function setRouteName(string $routeName): self
    {
        $this->routeName = $routeName;

        return $this;
    }

    protected function getRedirectUrl(): string
    {
        return route($this->redirectRouteName, $this->getRedirectParameters());
    }

    protected function getData(): array
    {
        return [];
    }
}
