<?php

namespace Beanz\Tests\Unit;

use App\Models\User;
use Auth;
use Beanz\Tests\UnitTestCase;
use Illuminate\Routing\Route as RoutingRoute;
use Mockery;
use Route;
use View;

abstract class ViewComposerTestCase extends UnitTestCase
{
    /** @var User */
    protected $user;

    /** @var View */
    protected $view;

    /** @var string */
    protected $viewName;

    /** @var Route */
    protected $routeMock;

    /** @var string */
    protected $route = 'test/checkViewComposer';

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->setUpTestEnvironment();
    }

    protected function setUpTestEnvironment(): void
    {
        view()->addNamespace('test_view', base_path('tests/resources/views'));

        $routes = Route::getRoutes();

        $this->routeMock = Mockery::mock(RoutingRoute::class)->makePartial();
        $this->routeMock->shouldReceive('parameters')->andReturn($this->getParameters());

        Route::shouldReceive('current')->andReturn($this->routeMock);
        Route::shouldReceive('getRoutes')->andReturn($routes);
        Route::makePartial();

        Auth::shouldReceive('user')->andReturn($this->user);

        $this->view = View::make('test_view::view-composer');
    }

    protected function setUpTestRoute(): void
    {
        Route::get(
            $this->route,
            function () {
                return response()->view(
                    $this->viewName,
                    [
                        'redirectRoute' => 'web.home',
                    ]
                );
            }
        );

        $this->actingAs($this->user);
    }
}
