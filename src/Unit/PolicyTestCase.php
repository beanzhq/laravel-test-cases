<?php

namespace Beanz\Tests\Unit;

use App\Models\User;
use Beanz\Tests\UnitTestCase;

abstract class PolicyTestCase extends UnitTestCase
{
    /** @var User */
    protected $user;

    /** @var string  */
    protected $policyClassName;

    /** @var object */
    protected $policy;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->policy = app($this->policyClassName);
    }

    abstract public function it_returns_false_when_access_is_denied(): void;
    abstract public function it_returns_true_when_access_is_granted(): void;
}
