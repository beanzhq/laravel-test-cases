<?php

namespace Beanz\Tests\Unit;

use Beanz\Basics\Repositories\RepositoryInterface;
use Beanz\Tests\UnitTestCase;

abstract class RepositoryTestCase extends UnitTestCase
{
    /** @var string */
    protected $repositoryInterface;

    /** @var RepositoryInterface */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = app($this->repositoryInterface);
    }
}
