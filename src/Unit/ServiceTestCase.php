<?php

namespace Beanz\Tests\Unit;

use Beanz\Tests\UnitTestCase;
use stdClass;

abstract class ServiceTestCase extends UnitTestCase
{
    /** @var stdClass */
    protected $service;

    /** @var string */
    protected $serviceName;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = class_exists($this->serviceName) ? app($this->serviceName) : null;
    }
}
