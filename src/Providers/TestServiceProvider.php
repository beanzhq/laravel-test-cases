<?php

namespace Beanz\Tests\Providers;

use Illuminate\Support\ServiceProvider;

class TestServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../bootstrap/autoload.php' => base_path('bootstrap/autoload.php'),
            __DIR__.'/../bootstrap/phpunit-autoload.php' => base_path('bootstrap/phpunit-autoload.php'),
            __DIR__.'/../bootstrap/phpunit-mysql-autoload.php' => base_path('bootstrap/phpunit-mysql-autoload.php'),
        ], 'bootstrap');

        $this->publishes([
            __DIR__.'/../example.phpunit.xml' => base_path('phpunit.xml'),
        ], 'phpunit');
    }
}
