<?php

use Beanz\Tests\TestBootstrap;

require __DIR__ . '/autoload.php';

(new TestBootstrap)->bootstrapMySQL();
