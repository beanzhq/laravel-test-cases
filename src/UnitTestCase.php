<?php

namespace Beanz\Tests;

use Cache;
use Exception;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

abstract class UnitTestCase extends TestCase
{
    use DatabaseTransactions {
        beginDatabaseTransaction as baseBeginDatabaseTransaction;
    }
    use WithFaker;

    /** @var bool */
    protected $mysqlTesting = false;

    /** @var string */
    private $originalMemoryLimit;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->originalMemoryLimit = ini_get('memory_limit');

        if (!env('PHP_UNIT_LOADED')) {
            throw new Exception('phpunit.xml not loaded.');
        }

        $this->mysqlTesting = env('DB_CONNECTION') === 'mysql_test';

        if (!$this->mysqlTesting) {
            (new TestBootstrap)->bootstrap();
        }

        parent::setUp();

        Cache::flush();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        ini_set('memory_limit', $this->originalMemoryLimit);
    }
}
