<?php

namespace Beanz\Tests;

use File;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Artisan;
use Tests\CreatesApplication;

class TestBootstrap
{
    use CreatesApplication;

    /** @var Application */
    protected $app = null;

    public function bootstrap(): void
    {
        if (getenv('SQLITE_BOOTSTRAP') !== false) {
            return;
        }

        if (!$this->app) {
            $this->app = $this->createApplication();
        }

        Artisan::call('up');

        $databasePath = storage_path('db-dumps');
        if(!File::isDirectory($databasePath)){
            File::makeDirectory($databasePath, 0755, true, true);
        }

        File::cleanDirectory(storage_path('db-dumps'));
        File::cleanDirectory(storage_path('tests'));
        File::cleanDirectory(base_path('tests/migrations'));

        File::put($databasePath . '/database.sqlite', '');
        Artisan::call('migrate:fresh');

        putenv('SQLITE_BOOTSTRAP=true');
    }

    public function bootstrapMySQL(): void
    {
        $this->createApplication();

        Artisan::call('migrate:fresh');
    }
}
